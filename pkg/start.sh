#!/bin/bash

set -eu

mkdir -p /app/data/plugins /run/grafana

if [[ ! -f /app/data/env ]]; then
  echo "=> Copying default configuration"
  cp /app/pkg/env /app/data/env
fi

if [[ ! -d /app/data/grafana ]]; then
  echo "=> Creating storage directory"
  mkdir -p /app/data/grafana
fi

if [[ ! -d /app/data/plugins ]]; then
  echo "=> Creating plugins directory"
  mkdir -p /app/data/plugins
fi

if [[ ! -d /app/data/plugins/grafana-image-renderer ]]; then
  echo "=> Installing grafana-image-renderer plugin"
  grafana-cli plugins install grafana-image-renderer
fi

echo "=> Loading configuration"
# These could be overridden by the end-user
export GF_ANALYTICS_CHECK_FOR_UPDATES="false"

# Load the end-user configuration
source /app/data/env

# These are non-overridable by the end-user to ensure a "working" cloudron app
export GF_DEFAULT_INSTANCE_NAME=${CLOUDRON_APP_DOMAIN}
## https://grafana.com/docs/grafana/latest/installation/configuration/#paths
export GF_PATHS_DATA="/app/data/grafana"
export GF_PATHS_PLUGINS="/app/data/plugins"
## https://grafana.com/docs/grafana/latest/installation/configuration/#server
export GF_SERVER_DOMAIN=${CLOUDRON_APP_DOMAIN}
export GF_SERVER_ENFORCE_DOMAIN="true"
export GF_SERVER_ROOT_URL=${CLOUDRON_APP_ORIGIN}
export GF_SERVER_ENABLE_GZIP="true"
## https://grafana.com/docs/grafana/latest/installation/configuration/#database
export GF_DATABASE_URL=${CLOUDRON_POSTGRESQL_URL}
## https://grafana.com/docs/grafana/latest/installation/configuration/#remote-cache
export GF_REMOTE_CACHE_TYPE="redis"
export GF_REMOTE_CACHE_CONNSTR="addr=${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT},pool_size=100,db=0,ssl=false"
## https://grafana.com/docs/grafana/latest/installation/configuration/#security
export GF_SECURITY_DISABLE_INITIAL_ADMIN_CREATION="true"
export GF_SECURITY_COOKIE_SECURE="true"
export GF_SECURITY_STRICT_TRANSPORT_SECURITY="true"
export GF_SECURITY_STRICT_TRANSPORT_SECURITY_PRELOAD="true"
export GF_SECURITY_X_CONTENT_TYPE_OPTIONS="true"
export GF_SECURITY_X_XSS_PROTECTION="true"
# export GF_SECURITY_
## https://grafana.com/docs/grafana/latest/auth/ldap/
export GF_AUTH_LDAP_ENABLED="true"
export GF_AUTH_LDAP_CONFIG_FILE="/app/pkg/ldap.toml"
export GF_AUTH_LDAP_ALLOW_SIGN_UP="/app/pkg/ldap.toml"
## https://grafana.com/docs/grafana/latest/installation/configuration/#smtp
export GF_SMTP_ENABLED="true"
export GF_SMTP_HOST="${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTPS_PORT}"
export GF_SMTP_USER=${CLOUDRON_MAIL_SMTP_USERNAME}
export GF_SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}
export GF_SMTP_SKIP_VERIFY="true"
export GF_SMTP_FROM_ADDRESS=${CLOUDRON_MAIL_FROM}
export GF_SMTP_EHLO_IDENTITY=${CLOUDRON_APP_HOSTNAME}
## https://grafana.com/docs/grafana/latest/installation/configuration/#log
export GF_LOG_MODE="console"

echo "=> Setting permissions"
chown -R grafana:grafana /app/data

echo "=> Starting Grafana"
exec grafana-server -homepath /usr/share/grafana

FROM cloudron/base:1.0.0

ARG GRAFANA_VERSION=6.7.3

RUN apt-get update && \
    apt-get install -y adduser libfontconfig1 && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN curl -SLf "https://dl.grafana.com/oss/release/grafana_${GRAFANA_VERSION}_amd64.deb" > /tmp/grafana.deb && \
    dpkg -i /tmp/grafana.deb && \
    rm /tmp/grafana.deb

RUN mkdir -p /app/pkg /app/data /run/grafana
RUN chown -R grafana:grafana /app/data /app/pkg /run/grafana

COPY --chown=grafana:grafana pkg/ /app/pkg/

WORKDIR /app/data

USER grafana

ENV PATH="/app/pkg/bin:$PATH"

CMD [ "/app/pkg/start.sh" ]
